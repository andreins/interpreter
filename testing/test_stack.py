import unittest
import jsinterp.error
from jsinterp.interpreter import Stack


class TestExecutionStack(unittest.TestCase):

    def setUp(self):
        self.stack = Stack()

    def test_peek(self):
        with self.assertRaises(jsinterp.error.EmptyStackError):
            self.stack.peek()
        self.stack.push(3)
        self.assertEqual(self.stack.peek(), 3)
        self.stack.push(5)
        self.assertEqual(self.stack.peek(), 5)

    def test_push(self):
        self.stack.push(3)
        self.assertEqual(self.stack.peek(), 3)
        self.stack.push(5)
        self.assertEqual(self.stack.peek(), 5)
        self.stack.pop()
        self.assertEqual(self.stack.peek(), 3)

    def test_pop(self):
        with self.assertRaises(jsinterp.error.EmptyStackError):
            self.stack.pop()
        self.assertNotEqual(self.stack.p, -1)
        self.stack.push(3)
        self.assertEqual(self.stack.pop(), 3)

    def test_stack_bounds(self):
        for i in xrange(1024):
            self.stack.push(i)
        with self.assertRaises(jsinterp.error.StackIndexOutOfBounds):
            self.stack.push(i)



if __name__ == '__main__':
    unittest.main()

