import unittest
import jsinterp.error
from jsinterp.interpreter import VM, Instruction, parse
from jsinterp.parser_v2 import gen_bytecode
from mock import patch
from StringIO import StringIO

INST_ADD = 0
INST_MULT = 1
INST_SUB = 2
INST_DIV = 3
INST_CMP = 4
INST_JUMP = 10
INST_JGT = 11
INST_PRINT = 12
INST_PUSH_VAR = 13
INST_DEF_VAR = 14
INST_GET_VAR = 15

class TestVM(unittest.TestCase):
    def setUp(self):
        self.vm = VM([])

    def test_ops(self):
        self.vm.instructions = [Instruction(INST_PUSH_VAR, [3]),
                                Instruction(INST_PUSH_VAR, [5]),
                                Instruction(INST_ADD),
                                Instruction(INST_PUSH_VAR, [10]),
                                Instruction(INST_PUSH_VAR, [5]),
                                Instruction(INST_SUB),
                                Instruction(INST_PUSH_VAR, [10]),
                                Instruction(INST_PUSH_VAR, [12]),
                                Instruction(INST_MULT),
                                Instruction(INST_PUSH_VAR, [15]),
                                Instruction(INST_PUSH_VAR, [5]),
                                Instruction(INST_DIV)
                                ]
        self.vm.execute()
        self.assertEqual(self.vm.stack.pop(), 3)
        self.assertEqual(self.vm.stack.pop(), 120)
        self.assertEqual(self.vm.stack.pop(), 5)
        self.assertEqual(self.vm.stack.pop(), 8)
        with self.assertRaises(jsinterp.error.EmptyStackError):
            self.vm.stack.pop()

    def test_cmp(self):
        self.vm.instructions = [Instruction(INST_PUSH_VAR, [0]),
                                Instruction(INST_PUSH_VAR, [2]),
                                Instruction(INST_CMP, [0])]
        self.vm.execute()
        self.assertEqual(self.vm.stack.peek(), 0)

    def test_jump(self):
        self.vm.instructions = [Instruction(INST_PUSH_VAR, [0]),
                                Instruction(INST_PUSH_VAR, [1]),
                                Instruction(INST_JUMP, [4]),
                                Instruction(INST_PUSH_VAR, [2]),
                                Instruction(INST_PUSH_VAR, [3]),
                                Instruction(INST_ADD),
                                ]
        self.vm.execute()
        self.assertEqual(self.vm.stack.peek(), 4)
        self.vm.pc = 0
        self.vm.instructions = [Instruction(INST_JUMP, [9])]
        with self.assertRaises(jsinterp.error.StackIndexOutOfBounds):
            self.vm.execute()

    def test_jgt(self):
        self.vm.instructions = parse(gen_bytecode("""
        var start = 10;
        var x = start;
        while ( 0 < x ) {
            x = x - 1;
        }
        """))
        self.vm.instructions.append(Instruction(INST_GET_VAR, ["x"]))
        self.vm.execute()
        self.assertEqual(self.vm.stack.peek(), 0)
        self.vm.pc = 0
        self.vm.instructions = [Instruction(INST_JGT, [10])]
        with self.assertRaises(jsinterp.error.StackIndexOutOfBounds):
            self.vm.execute()

    @patch('sys.stdout', new_callable=StringIO)
    def test_while_nest(self, mock_stdout):
        self.vm.instructions = parse(gen_bytecode("""
        var start = 3;
        var x = start;
        var y = start;
        var z = start;
        while ( 0 < x ) {
            while ( 0 < y ) {
                while ( 0 < z ) {
                    z = z - 1;
                }
                y = y - 1;
                console.log(y);
                z = start;
            }
            x = x - 1;
            y = start;
        }
        """))
        self.vm.execute()
        answer = "2\n1\n0\n2\n1\n0\n2\n1\n0\n"
        self.assertEqual(mock_stdout.getvalue(), answer)

    def test_def_get(self):
        self.vm.instructions = [Instruction(INST_PUSH_VAR, [3]),
                                Instruction(INST_DEF_VAR, "x"),
                                Instruction(INST_GET_VAR, "x")]
        self.vm.execute()
        self.assertEqual(self.vm.stack.peek(), 3)



if __name__ == '__main__':
    unittest.main()

