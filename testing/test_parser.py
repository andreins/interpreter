import unittest
from jsinterp.parser_v2 import ASTParser, gen_ast, gen_bytecode

class TestParser(unittest.TestCase):

    def setUp(self):
        self.input = """
        var x = (5 + 2) - 3;
        var y = x;
        var z = (x + y) - 1;
        while ( x > 0 * 135 ) {
            x = x - 1 - 0 * 1;
        }
        """

    def test_ASTParser(self):
        x = ASTParser(gen_ast(self.input)).bclist
        answer = "13 5\n13 2\n0\n13 3\n2\n14 x\n15 x\n14 y\n15 x\n15 y\n0\n13 1\n2\n14 z\nWRET 1\n15 x\n13 0\n13 135\n1\n4 0\nWJGT 1\n15 x\n13 1\n2\n13 0\n13 1\n1\n2\n14 x\nWJUMP 1\n"
        self.assertEqual(x, answer)

    def test_bytecode_gen(self):
        x = gen_bytecode(self.input)
        answer = "13 5\n13 2\n0\n13 3\n2\n14 x\n15 x\n14 y\n15 x\n15 y\n0\n13 1\n2\n14 z\n15 x\n13 0\n13 135\n1\n4 0\n11 29\n15 x\n13 1\n2\n13 0\n13 1\n1\n2\n14 x\n10 14\n"
        self.assertEqual(x, answer)

