from slimit.parser import Parser
import sys
import os

INST_ADD = 0
INST_MULT = 1
INST_SUB = 2
INST_DIV = 3
INST_CMP = 4
INST_EQUAL = 5
INST_NEQUAL = 6
INST_JUMP = 10
INST_JGT = 11
INST_PRINT = 12
INST_PUSH_VAR = 13
INST_PUSH_STR = 132
INST_PUSH_BOOL = 133
INST_DEF_VAR = 14
INST_DEF_OBJ = 142
INST_DEF_FUNC = 143
INST_DEF_OBJ_ATTR = 144
INST_DEF_ARRAY = 145
INST_DEF_ARRAY_SLOT = 146
INST_GET_VAR = 15
INST_GET_OBJ_ATTR = 152
INST_GET_FUNC = 153
INST_GET_ARRAY_SLOT = 154
INST_FUNC_RETURN = 16
MARK_WHILE_RETURN = 167
MARK_WJUMP = 17
MARK_WJGT = 18
MARK_OBJ_START = 19
MARK_OBJ_END = 20
MARK_FUNC_START = 21
MARK_FUNC_END = 22
MARK_IF_END = 23
MARK_IF_ALT = 24
MARK_IF_CONS = 25

try:
    os.remove("output")
except OSError:
    pass
os.open("output", os.O_CREAT, 0777)
output_file = os.open("output", os.O_RDWR, 0777)


class FuncCounter:
    def __init__(self):
        self.func_count = 0
        self.open = 0
        self.closed = 0

    def open_func(self):
        self.func_count = self.closed + self.open + 1
        self.open += 1
        return str(self.func_count)

    def close_func(self):
        toRet = self.func_count
        self.open -= 1
        self.closed += 1
        return str(toRet)
_fcounter = FuncCounter()


class WhileCounter:
    def __init__(self):
        self.while_count = 0
        self.open = 0
        self.closed = 0

    def mark_jump(self):
        return str(self.while_count) #+ str(self.total_whiles)

    def open_while(self):
        self.while_count = self.closed + self.open + 1
        self.open += 1
        return str(self.while_count) #+ str(self.total_whiles)

    def close_while(self):
        toRet = str(self.while_count) #+ str(self.total_whiles)
        self.while_count -= 1
        self.closed += 1
        self.open -= 1
        return str(toRet)
_wcounter = WhileCounter()


class IfCounter:
    def __init__(self):
        self.if_count = 0
        self.open = 0
        self.closed = 0

    def mark_if(self):
        return str(self.if_count)

    def open_if(self):
        self.if_count = self.closed + self.open + 1
        self.open += 1
        return str(self.if_count)

    def close_if(self):
        toRet = self.if_count
        self.if_count -= 1
        self.closed += 1
        self.open -= 1
        return str(toRet)
_ifcounter = IfCounter()


class ObjDef:
    def __init__(self):
        self.declaration_count = 0
        self.obj_declaration = False
        self.obj_declared = 0

    def add_def(self):
        if self.obj_declaration:
            self.declaration_count += 1

    def start_def(self):
        self.obj_declaration = True
        self.obj_declared += 1

    def end_def(self):
        self.obj_declaration = False
        self.declaration_count = 0

    def get_counter(self):
        return str(self.declaration_count)

    def get_obj_number(self):
        return str(self.obj_declared)
_objdef = ObjDef()


def count_extra(lines, e):
    x = 0
    b = 0
    extra_marks = ["WRET", "OBJEND", "IFEND", "FEND"]
    while b <= e:
        if lines[b].split(' ')[0] in extra_marks:
            x += 1
        b += 1
    return x


def gen_return_address(pc):
    lines = pc.split('\n')
    extra_marks = ["WRET", "OBJEND", "IFEND", "FEND"]
    lines_copy = list(filter(lambda x: x.split(' ')[0] not in extra_marks, lines))
    return_addresses = {}

    for line in lines:
        line_string = line
        line = line.split(' ')
        # inst JUMP = 10; JGT = 11
        if line[0] == "WRET":
            end = lines.index("WRET " + line[1])
            extra = count_extra(lines, end) - 1
            return_addresses[line[1]] = str(end - extra)
        elif line[0] == "WJUMP":
            lines_copy[lines_copy.index("WJUMP " + line[1])] = "10 " + return_addresses[line[1]]
        if line[0] == "WJGT":
            lines_copy[lines_copy.index("WJGT " + line[1])] = "11 " + str(lines_copy.index("WJUMP " + line[1]) + 1)
        if line[0] == "OBJSTART":
            end = next((s.split(' ') for s in lines if "OBJEND " + line[1] in s), None)
            lines_copy[lines_copy.index("OBJSTART " + line[1])] = "142 " + str(end[2]) + ' ' + str(end[3])
        elif line[0] == "FSTART":
            key = "FEND " + line[1] + ' ' + line[2]
            end = lines.index(key) - 1
            extra = count_extra(lines, end)
            params = ' '.join(line[2:])

            lines_copy[lines_copy.index(' '.join(line))] = "143 " + str(end - extra) + " " + params
        elif line[0] == "IFA":
            lines_copy[lines_copy.index(' '.join(line))] = "11 " + str(lines_copy.index("IFC " + line[1]) + 1)
        elif line[0] == "IFC":
            end = lines.index("IFEND " + line[1])
            extra = count_extra(lines, end - 1)
            lines_copy[lines_copy.index(' '.join(line))] = "10 " + str(end - extra)

    return '\n'.join(lines_copy)


class Instruction:
    def __init__(self, opcode, args=[]):
        self.opcode = opcode
        self.args = args

    def get_args(self):
        x = ""
        for arg in self.args:
            x += str(arg) + ' '
        if x is not "":
            x = x[:-1]
        return x

    def translate(self, bclist):
        args = "" if self.get_args() == "" else " " + str(self.get_args())
        x = str(self.opcode) + args + '\n'
        if self.opcode == MARK_WHILE_RETURN:
            x = "WRET " + _wcounter.open_while() + '\n'
        elif self.opcode == MARK_WJUMP:
            x = "WJUMP " + _wcounter.close_while() + '\n'
        elif self.opcode == MARK_WJGT:
            x = "WJGT " + _wcounter.mark_jump() + '\n'
        elif self.opcode == INST_DEF_VAR:
            _objdef.add_def()
        elif self.opcode == MARK_OBJ_END:
            x = "OBJEND " + _objdef.get_obj_number() + args + ' ' + _objdef.get_counter() + '\n'
            _objdef.end_def()
        elif self.opcode == MARK_OBJ_START:
            _objdef.start_def()
            x = "OBJSTART " + _objdef.get_obj_number() + '\n'
        elif self.opcode == MARK_FUNC_START:
            x = "FSTART " + _fcounter.open_func() + args + '\n'
        elif self.opcode == MARK_FUNC_END:
            x = "FEND " + _fcounter.close_func() + args + '\n'
        elif self.opcode == MARK_IF_ALT:
            x = "IFA " + _ifcounter.open_if() + '\n'
        elif self.opcode == MARK_IF_CONS:
            x = "IFC " + _ifcounter.mark_if() + '\n'
        elif self.opcode == MARK_IF_END:
            x = "IFEND " + _ifcounter.close_if() + '\n'
        bclist.append(x)


def gen_ast(input):
    return Parser().parse(input).children()


class ASTParser:
    def __init__(self, ast):
        self.ast_map = {'VarStatement': self.parse_var, 'Identifier': self.parse_identifier,
                   'Number': self.parse_num, 'While': self.parse_while, 'BinOp': self.parse_binop,
                   'Block': self.parse_block, 'ExprStatement': self.parse_expr, 'Object': self.parse_object,
                   'DotAccessor': self.parse_expr, 'FuncDecl': self.parse_func, 'Return': self.parse_return,
                    'FunctionCall': self.parse_expr, 'If': self.parse_if, 'String': self.parse_string,
                    'Boolean': self.parse_boolean, 'Array': self.parse_array, 'BracketAccessor': self.parse_bracket_accessor}
        self.instructions = []
        self.bclist = []
        [self.parse(node) for node in ast]
        [instruction.translate(self.bclist) for instruction in self.instructions]
        self.bclist = ''.join(self.bclist)

    # generic parser
    def parse(self, node):
        self.ast_map[get_node_type(node)](node)

    # variable declaration
    def parse_var(self, node):
        decl = node.children()[0]
        if get_node_type(decl.initializer) == "Array":
            self.parse_array(decl, True)
        else:
            self.parse(decl.initializer)
            self.parse_identifier(decl.identifier, True, get_node_type(decl.initializer) == 'Object')

    def parse_assign(self, node):
        self.parse(node.right)
        self.parse_identifier(node.left, True)

    def parse_object(self, obj):
        self.instructions.append(Instruction(MARK_OBJ_START))
        [self.parse_assign(assign) for assign in obj]

    def parse_array(self, node, defining=False):
        l = []
        for item in node.initializer.items:
            l.append(item.value)
        self.instructions.append(Instruction(INST_DEF_ARRAY, [node.identifier.value] + l))


    # operation between two elements
    def parse_binop(self, node):
        self.parse(node.left)
        self.parse(node.right)
        self.parse_op(node.op)

    def parse_block(self, nodes):
        [self.parse(node) for node in nodes]

    def parse_num(self, node):
        self.instructions.append(Instruction(INST_PUSH_VAR, [node.value]))

    def parse_boolean(self, node):
        self.instructions.append(Instruction(INST_PUSH_BOOL, [node.value]))

    def parse_string(self, node):
        self.instructions.append(Instruction(INST_PUSH_STR, [node.value]))

    def parse_identifier(self, node, defining=False, object=False):
        if object:
            self.instructions.append(Instruction(MARK_OBJ_END, [node.value]))
        else:
            if get_node_type(node) == "BracketAccessor":
                self.parse_bracket_accessor(node, defining)
            elif get_node_type(node) == "DotAccessor":
                self.instructions.append(Instruction(INST_DEF_OBJ_ATTR, [node.node.value, node.identifier.value]))
            else:
                self.instructions.append(Instruction(INST_DEF_VAR if defining else INST_GET_VAR, [node.value]))

    def parse_bracket_accessor(self, node, defining=False):
        self.parse(node.expr)
        if defining:
            self.instructions.append(Instruction(INST_DEF_ARRAY_SLOT, [node.node.value]))
        else:
            self.instructions.append(Instruction(INST_GET_ARRAY_SLOT, [node.node.value]))


    def parse_func(self, node):
        params = ""
        if len(node.parameters) is not 0:
            for parameter in node.parameters:
                params += parameter.value + ' '
            params = params[:-1]
        self.instructions.append(Instruction(MARK_FUNC_START, [node.identifier.value, params]))
        self.parse_block(node.elements)
        self.instructions.append(Instruction(MARK_FUNC_END, [node.identifier.value]))

    def parse_return(self, node):
        self.parse(node.expr)
        self.instructions.append(Instruction(INST_FUNC_RETURN))

    def parse_expr(self, node):
        try:
            node = node.expr
        except AttributeError:
            pass
        if get_node_type(node) == 'Assign':
            if get_node_type(node.right) == 'Array':
                node.initializer = node.right
                node.identifier = node.left
                self.parse_array(node)
            else:
                self.parse(node.right)
                self.parse_identifier(node.left, True)
        elif get_node_type(node) == 'FunctionCall':
            [self.parse(x) for x in node.args]
            if get_node_type(node.identifier) == "Identifier":
                self.instructions.append(Instruction(INST_GET_FUNC, [node.identifier.value, len(node.args)]))
            else:
                obj_name = node.identifier.node.value
                obj_attr = node.identifier.identifier.value
                self.instructions.append(Instruction(INST_GET_OBJ_ATTR, [obj_name, obj_attr, len(node.args)]))

        elif get_node_type(node) == 'DotAccessor':
            obj_name = node.node.value
            obj_attr = node.identifier.value
            self.instructions.append(Instruction(INST_GET_OBJ_ATTR, [obj_name, obj_attr]))

    def parse_while(self, node):
        self.instructions.append(Instruction(MARK_WHILE_RETURN))
        self.parse_binop(node.predicate)
        self.instructions.append(Instruction(MARK_WJGT))
        self.parse_block(node.statement.children())
        self.instructions.append(Instruction(MARK_WJUMP))

    def parse_if(self, node):
        self.parse_binop(node.predicate)
        self.instructions.append(Instruction(MARK_IF_ALT))
        self.parse_block(node.consequent)
        self.instructions.append(Instruction(MARK_IF_CONS))
        self.parse_block(node.alternative)
        self.instructions.append(Instruction(MARK_IF_END))

    def parse_op(self, op):
        args = []
        if op == '+':
            instr = INST_ADD
        elif op == '-':
            instr = INST_SUB
        elif op == '*':
            instr = INST_MULT
        elif op == '/':
            instr = INST_DIV
        elif op == '<':
            instr = INST_CMP
            args = [1]
        elif op == '>':
            instr = INST_CMP
            args = [0]
        elif op == '===':
            instr = INST_EQUAL
        elif op == '!==':
            instr = INST_NEQUAL
        self.instructions.append(Instruction(instr, args))


def get_node_type(node):
    return type(node).__name__

def gen_bytecode(pc):
    return gen_return_address(ASTParser(gen_ast(pc)).bclist)


def run(fp):
    program_contents = ""
    while True:
        read = os.read(fp, 4096)
        if len(read) == 0:
            break
        program_contents += read
    os.close(fp)
    os.write(output_file, gen_bytecode(program_contents))
    os.close(output_file)


def entry_point(argv):
    try:
        filename = argv[1]
    except IndexError:
        print "You must supply a filename"
        return 1

    run(os.open(filename, os.O_RDONLY, 0777))
    return 0


if __name__ == "__main__":
    entry_point(sys.argv)
