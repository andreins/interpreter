import os
import error
from rpython.rlib.jit import JitDriver, elidable, promote, elidable_promote

def get_printable_location(pc, instructions, self):
    return "%d:%s" % (pc, instructions[pc].str())
jitdriver = JitDriver(greens=['pc', 'instructions', 'self'], reds=['instr_len','stack'], get_printable_location=get_printable_location)

INST_ADD = 0
INST_MULT = 1
INST_SUB = 2
INST_DIV = 3
INST_CMP = 4
INST_EQUAL = 5
INST_NEQUAL = 6
INST_JUMP = 10
INST_JGT = 11
INST_PRINT = 12
INST_PUSH_VAR = 13
INST_PUSH_STR = 132
INST_PUSH_BOOL = 133
INST_DEF_VAR = 14
INST_DEF_OBJ = 142
INST_DEF_FUNC = 143
INST_DEF_OBJ_ATTR = 144
INST_DEF_ARRAY = 145
INST_DEF_ARRAY_SLOT = 146
INST_GET_VAR = 15
INST_GET_OBJ_ATTR = 152
INST_GET_FUNC = 153
INST_GET_ARRAY_SLOT = 154
INST_FUNC_RETURN = 16
inst_name_map = {0 : "ADD", 1 : "MULTIPLY", 2 : "SUBSTITUTE",
                 3 : "DIV", 4 : "COMPARE", 10 : "JUMP", 11 : "JGT",
                 12: "PRINT", 13: "PUSH VAR", 14: "DEFINE VAR",
                 15: "GET VAR"}

class VersionTag(object):
    pass


class Map(object):
    def __init__(self):
        self.attribute_indexes = {}
        self.other_maps = {}

    @elidable_promote()
    def getindex(self, name):
        return self.attribute_indexes.get(name, -1)

    @elidable
    def new_map_with_additional_attribute(self, name):
        if name not in self.other_maps:
            newmap = Map()
            newmap.attribute_indexes.update(self.attribute_indexes)
            newmap.attribute_indexes[name] = len(self.attribute_indexes)
            self.other_maps[name] = newmap
        return self.other_maps[name]
EMPTY_MAP = Map()


class W_Instance(object):
    def __init__(self):
        self.map = EMPTY_MAP
        self.storage = []

    @elidable
    def _getfield(self, name):
        map = promote(self.map)
        return map.getindex(name)

    def getfield(self, name):
        map = promote(self.map)
        index = map.getindex(name)
        if index != -1:
            return self.storage[index]
        raise AttributeError(name)

    def write_attribute(self, name, value):
        map = promote(self.map)
        index = map.getindex(name)
        if index != -1:
            self.storage[index] = value
            return
        self.map = map.new_map_with_additional_attribute(name)
        self.storage.append(value)


    def getattr(self, name):
        return self.getfield(name)


class W_Object(W_Instance):
    def __init__(self):
        W_Instance.__init__(self)

class W_Array(W_Object):
    def __init__(self, l):
        W_Object.__init__(self)
        self.l = l
        self.write_attribute("push", Array_Append(self))
        self.write_attribute("permutations", Array_Perm(self))
        self.write_attribute("len", Array_Len(self))

    def set_slot(self, index, value):
        assert isinstance(index, Number)
        if index.value > len(self.l):
            self.l = [Undefined()] * index.value + [value]
        elif index.value == len(self.l):
            self.l.append(value)
        else:
            self.l[index.value] = value

    def array_append(self, value):
        self.l = self.l + [value]

    def get_slot(self, index):
        assert isinstance(index, Number)
        return self.l[index.value]

    def str(self):
        if len(self.l) == 0:
            return "[]"
        s = "["
        for v in self.l:
            s += v.str() + ", "
        i = len(s) - 2
        assert i >= 0
        return s[:i] + "]"

class W_Function(W_Object):
    def __init__(self):
        W_Object.__init__(self)
        self.start = 0
        self.params = []

    def set_params(self, start, params):
        self.start = start
        self.params = params

    def call(self, params, parent, ret_addr, stack):
        frame = Frame(self.params, parent)
        frame.push(params, ret_addr)
        return frame, frame.scope, self.start

class Array_Append(W_Function):
    def __init__(self, array):
        W_Function.__init__(self)
        assert isinstance(array, W_Array)
        self.array = array

    def call(self, params, parent, ret_addr, stack=None):
        if len(params) > 0:
            self.array.array_append(params[0])
        return parent, parent.scope, ret_addr - 1

def all_perms(elements):
    if len(elements) <=1:
        yield elements
    else:
        for perm in all_perms(elements[1:]):
            for i in range(len(elements)):
                assert(i <= len(perm))
                yield perm[:i] + elements[0:1] + perm[i:]

class Array_Perm(W_Function):
    def __init__(self, array):
        W_Function.__init__(self)
        assert isinstance(array, W_Array)
        self.array = array

    def call(self, params, parent, ret_addr, stack):
        assert(isinstance(stack, Stack))
        l = []
        for perm in all_perms(self.array.l):
            l.append(W_Array(perm))
        stack.push(W_Array(l))
        return parent, parent.scope, ret_addr - 1

class Array_Len(W_Function):
    def __init__(self, array):
        W_Function.__init__(self)
        assert isinstance(array, W_Array)
        self.array = array

    def call(self, params, parent, ret_addr, stack):
        assert(isinstance(stack, Stack))
        stack.push(Number(len(self.array.l)))
        return parent, parent.scope, ret_addr - 1

class STL_Log(W_Function):
    def __init__(self):
        W_Function.__init__(self)

    def call(self, params, parent, ret_addr, stack):
        if len(params) > 0:
            print params[0].str()
        return parent, parent.scope, ret_addr - 1

class STL_Read(W_Function):
    def __init__(self):
        W_Function.__init__(self)

    def call(self, params, parent, ret_addr, stack):
        assert(isinstance(parent, Frame))
        assert(isinstance(stack, Stack))
        if len(params) > 0:
            print(params[0].str())
        stack.push(cast_var(os.read(0, 4096)))
        return parent, parent.scope, ret_addr - 1


class UserFunctions(object):
    def __init__(self):
        self.instance = W_Instance()
        self.load_console()
        self.load_readln()

    def load_console(self):
        console = W_Object()
        console.write_attribute("log", STL_Log())
        self.instance.write_attribute("console", console)

    def load_readln(self):
        self.instance.write_attribute("readln", STL_Read())

    def get_stl(self):
        f = Frame(None, None)
        f.scope = self.instance
        return f

class Frame(object):
    def __init__(self, params, parent):
        self.p_name = params
        self.parent = parent
        self.ret_addr = -1
        self.scope = W_Instance()

    def push(self, p_val, ret_addr):
        self.ret_addr = ret_addr
        for i in range(len(p_val)):
            try:
                x = self.p_name[i]
            except IndexError:
                x = None
            y = p_val[len(p_val) - i - 1]
            self.scope.write_attribute(x, y)

    def getattr(self, name):
        try:
            return self.scope.getattr(name)
        except AttributeError:
            return self.parent.getattr(name)


    def pop(self):
        return self.parent, self.ret_addr, self.parent.scope


class Instruction(object):
    _immutable_fields_ = ["opcode", "args[*]"]

    def __init__(self, opcode, args=[]):
        self.opcode = opcode
        self.args = args

    def get_args(self):
        return self.args[0]

    def str(self):
        return inst_name_map[self.opcode]


class VM(object):
    _immutable_fields_ = ["instructions"]

    def __init__(self, instructions):
        stl = UserFunctions().get_stl()
        self.instructions = instructions
        self.caller_instance = None
        self.inst = W_Instance()
        self.main_frame = Frame(None, stl)
        self.main_frame.scope = self.inst
        self.stack = Stack("Main")
        self.current_frame = self.main_frame
        self.obj_helper = -1
        self.pc = 0

    def adjust_state(self):
        if self.obj_helper is not -1:
            if self.obj_helper == 1:
                self.obj_helper = -1
                self.inst = self.caller_instance
            else:
                self.obj_helper = self.obj_helper - 1

    def format_args(self, args):
        vars = []
        for var in args:
            vars.append(cast_var(var, self.main_frame))
        return vars

    def execute(self):
        instr_len = len(self.instructions)
        while self.pc < instr_len:
            jitdriver.jit_merge_point(
                pc=self.pc, instr_len=instr_len, stack=self.stack, instructions=self.instructions, self=self)
            instr = self.instructions[self.pc]
            if instr.opcode == INST_PUSH_VAR:
                self.stack.push(Number(int(instr.get_args())))
            elif instr.opcode == INST_PUSH_STR:
                str = format_str(instr.get_args())
                self.stack.push(String(str))
            elif instr.opcode == INST_PUSH_BOOL:
                if instr.get_args() == "true":
                    self.stack.push(Bool(True))
                else:
                    self.stack.push(Bool(False))
            elif instr.opcode == INST_JUMP:
                if int(instr.get_args()) - 1 > instr_len:
                    raise error.StackIndexOutOfBounds("Attempted to call jump outside stack bounds")
                self.pc = int(instr.get_args()) - 1
            elif instr.opcode == INST_CMP:
                if int(instr.get_args()) == 0:
                    if self.stack.pop().lt(self.stack.pop()).eval():
                        self.stack.push(Bool(True))
                    else:
                        self.stack.push(Bool(False))
                else:
                    if self.stack.pop().gt(self.stack.pop()).eval():
                        self.stack.push(Bool(True))
                    else:
                        self.stack.push(Bool(False))
            elif instr.opcode == INST_EQUAL:
                if self.stack.pop().eq(self.stack.pop()).eval():
                   self.stack.push(Bool(True))
                else:
                    self.stack.push(Bool(False))
            elif instr.opcode == INST_NEQUAL:
                if self.stack.pop().eq(self.stack.pop()).eval():
                    self.stack.push(Bool(False))
                else:
                    self.stack.push(Bool(True))
            elif instr.opcode == INST_JGT:
                if int(instr.get_args()) - 1 > instr_len:
                    raise error.StackIndexOutOfBounds("Attempted to call JGT outside of stack bounds")
                if not self.stack.pop().eval():
                    self.pc = int(instr.get_args()) - 1
            elif instr.opcode == INST_DEF_VAR:
                self.inst.write_attribute(instr.get_args(), self.stack.pop())
                self.adjust_state()
            elif instr.opcode == INST_DEF_OBJ:
                if int(instr.args[1]) is 0:
                    self.inst.write_attribute(instr.args[0], W_Object())
                else:
                    self.obj_helper = int(instr.args[1])
                    obj_inst = W_Object()
                    self.inst.write_attribute(instr.args[0], obj_inst)
                    self.caller_instance = self.inst
                    self.inst = obj_inst
            elif instr.opcode == INST_DEF_ARRAY:
                if len(instr.args) == 1:
                    self.inst.write_attribute(instr.args[0], W_Array([]))
                else:
                    self.inst.write_attribute(instr.args[0], W_Array(self.format_args(instr.args[1:])))
            elif instr.opcode == INST_DEF_ARRAY_SLOT:
                index = self.stack.pop()
                value = self.stack.pop()
                array = self.inst.getattr(instr.args[0])
                assert isinstance(array, W_Array)
                array.set_slot(index, value)
            elif instr.opcode == INST_GET_ARRAY_SLOT:
                index = self.stack.pop()
                array = self.inst.getattr(instr.args[0])
                assert isinstance(array, W_Array)
                self.stack.push(array.get_slot(index))
            elif instr.opcode == INST_DEF_OBJ_ATTR:
                self.inst.getattr(instr.args[0]).write_attribute(instr.args[1], self.stack.pop())
            elif instr.opcode == INST_GET_VAR:
                var = self.current_frame.getattr(instr.get_args())
                self.stack.push(var)
            elif instr.opcode == INST_GET_OBJ_ATTR:
                if len(instr.args) == 3: #func call
                    params = []
                    for x in xrange(int(instr.args[2])):
                        params.append(self.stack.pop())
                    func = self.current_frame.getattr(instr.args[0]).getattr(instr.args[1])
                    (frame, inst, index) = func.call(params, self.current_frame, self.pc + 1, self.stack)
                    self.current_frame = frame
                    self.inst = inst
                    self.pc = index
                else:
                    self.stack.push(self.inst.getattr(instr.args[0]).getattr(instr.args[1]))
            elif instr.opcode == INST_DEF_FUNC:
                end_pc = int(instr.args[0])
                name = instr.args[1]
                params = instr.args[2:]
                func = W_Function()
                func.set_params(self.pc, params)
                self.inst.write_attribute(name, func)
                self.adjust_state()
                self.pc = end_pc
            elif instr.opcode == INST_GET_FUNC:
                params = []
                for x in xrange(int(instr.args[1])):
                    params.append(self.stack.pop())
                func = self.current_frame.getattr(instr.args[0])
                (frame, inst, index) = func.call(params, self.current_frame, self.pc + 1, self.stack)
                self.current_frame = frame
                self.inst = inst
                self.pc = index
            elif instr.opcode == INST_FUNC_RETURN:
                (caller, pc, inst) = self.current_frame.pop()
                self.current_frame = caller
                self.pc = pc - 1
                self.inst = inst
            elif instr.opcode == INST_DIV:
                top = self.stack.pop()
                self.stack.push(self.stack.pop().div(top))
            elif instr.opcode == INST_ADD:
                top = self.stack.pop()
                self.stack.push(self.stack.pop().add(top))
            elif instr.opcode == INST_MULT:
                self.stack.push(self.stack.pop().mul(self.stack.pop()))
            elif instr.opcode == INST_PRINT:
                print self.stack.pop()
            elif instr.opcode == INST_SUB:
                top = self.stack.pop()
                self.stack.push(self.stack.pop().sub(top))
            self.pc += 1
        pass


class PrimWrapper(W_Object):
    def __init__(self):
        W_Object.__init__(self)

class NaN(PrimWrapper):
    def __init__(self):
        PrimWrapper.__init__(self)

    def str(self):
        return "NaN"

class Undefined(PrimWrapper):
    def __init__(self):
        PrimWrapper.__init__(self)

    def add(self, other):
        return NaN()

    def sub(self, other):
        return NaN()

    def mul(self, other):
        return NaN()

    def div(self, other):
        return NaN()

    def gt(self, other):
        return Bool(False)

    def lt(self, other):
        return Bool(False)

    def eq(self, other):
        return Bool(False)

    def eval(self):
        return False

    def str(self):
        return "undefined"

class Bool(PrimWrapper):
    def __init__(self, value=None):
        PrimWrapper.__init__(self)
        self.value = True if value is True else False

    def eval(self):
        return self.value

class Number(PrimWrapper):
    _immutable_fields_ = ["value"]
    def __init__(self, value=None):
        PrimWrapper.__init__(self)
        self.value = value

    def add(self, other):
        if isinstance(other, Number):
            return Number(self.value + other.value)
        else:
            assert isinstance(other, String)
            return String(str(self.value) + other.value)

    def sub(self, other):
        if isinstance(other, Number):
            return Number(self.value - other.value)
        else:
            return NaN()

    def mul(self, other):
        if isinstance(other, Number):
            return Number(self.value * other.value)
        else:
            return NaN()

    def div(self, other):
        if isinstance(other, Number):
            return Number(self.value / other.value)
        else:
            return NaN()

    def gt(self, other):
        if isinstance(other, Number):
            return Bool(self.value > other.value)
        else:
            return Bool(False)

    def lt(self, other):
        if isinstance(other, Number):
            return Bool(self.value < other.value)
        else:
            return Bool(False)

    def eq(self, other):
        if isinstance(other, Number):
            return Bool(self.value == other.value)
        else:
            return Bool(False)

    def str(self):
        return str(self.value)



class String(PrimWrapper):
    def __init__(self, value=None):
        PrimWrapper.__init__(self)
        self.value = value

    def str(self):
        return self.value

    def add(self, other):
        if isinstance(other, String):
            return String(self.value + other.value)
        else:
            assert isinstance(other, Number)
            return String(self.value + str(other.value))

    def eq(self, other):
        if isinstance(other, String):
            return Bool(self.value == other.value)
        else:
            return Bool(False)

    def sub(self, other):
        return NaN()

    def mul(self, other):
        return NaN()

    def div(self, other):
        return NaN()


class Stack:
    def __init__(self, name):
        self.s_obj = Undefined()
        self.stack_size = 1024
        self.stack = [self.s_obj] * self.stack_size
        self.p = 0
        self.maxoccup = 0

    def peek(self):
        if self.p == 0:
            raise error.EmptyStackError("Peek call on an empty stack")
        return self.stack[self.p - 1]

    def push(self, v):
        try:
            self.stack[self.p] = v
        except IndexError:
            raise error.StackIndexOutOfBounds("Stack out of memory")
        self.p += 1
        # self.maxoccup = max(self.p, self.maxoccup)

    def pop(self):
        if self.p == 0:
            raise error.EmptyStackError("Pop call on empty stack")
        self.p -= 1
        v = self.stack[self.p]
        self.stack[self.p] = self.s_obj
        return v

def get_instruction(instruction):
    if '"' in instruction:
        end_index = str(instruction).find('"') - 1
        if end_index > 0:
            opcode = instruction[0:end_index]
            instr = instruction[end_index + 1 : len(instruction)]
            return Instruction(int(opcode), [instr])
    else:
        instruction = instruction.split(' ')
        if len(instruction) == 1:
            return Instruction(int(instruction[0]))
        return Instruction(int(instruction[0]), instruction[1:])

def parse(pc):
    lines = pc.split('\n')
    return [get_instruction(line) for line in lines[:-1]]

def run(args):
    program_contents = ""
    output_file = os.open("output", os.O_RDONLY, 0777)
    while True:
        read = os.read(output_file, 4096)
        if len(read) == 0:
            break
        program_contents += read
    os.close(output_file)
    instructions = parse(program_contents)
    VM(instructions).execute()
    return 0


def cast_var(var, frame=None):
    if var.endswith('\n'):
        i = len(var) - 1
        assert(0 < i and len(var) > i)
        var = var[:i]
        if var.isdigit():
            return Number(int(var))
        return String(var)
    if var.isdigit():
        return Number(int(var))
    else:
        if len(var) > 0 and var[0] == '"':
            return String(format_str(var))
        else:
            assert(isinstance(frame, Frame))
            return frame.getattr(var)

def format_str(str):
    str = str[1:]
    if len(str) > 0:
        return str[:-1]
    return str


def jitpolicy(driver):
    from rpython.jit.codewriter.policy import JitPolicy
    return JitPolicy()


def target(*args):
    return run, None

if __name__ == "__main__":
    run("")
