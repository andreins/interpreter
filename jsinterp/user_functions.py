from interpreter import parse

log = "143 3 __log text\n" \
      "15 text\n" \
      "12\n" \
      "16\n" \
      "142 console 1\n" \
      "15 __log\n" \
      "14 log\n"

variables = {log: log}


class Loader(object):
    def __init__(self):
        self.variables = variables
        self.size = self.get_stack_size()
        self.pl_instr = self.get_objects()

    def get_objects(self):
        x = []
        for variable in self.variables:
            x.append(parse(variable))
        return [item for sublist in x for item in sublist]

    def format_instructions(self, instructions):
        for instruction in instructions:
            if instruction.opcode in [10, 11, 143]:
                instruction.args[0] = str(int(instruction.args[0]) + self.size)
        return self.pl_instr + instructions

    def get_stack_size(self):
        size = 0
        for variable in self.variables:
            size += len(variable.split('\n'))
        return size - 1

