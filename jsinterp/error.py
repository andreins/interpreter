
class InterpreterError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        "NOT_RPYTHON"
        return self.msg

class StackIndexOutOfBounds(InterpreterError):
    pass

class EmptyStackError(InterpreterError):
    pass


