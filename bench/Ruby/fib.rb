def fib(x)
  return 0 if x == 0
  return 1 if x == 1
  fib(x-1) + fib(x-2)
end
fib(30)
