def reverse(n, l)
    return [] if n <= 0
    l[0..n - 1].reverse! + l[n..l.size()]
end

def max(a,b)
  if a > b
    return a
  end
  b
end

def fannkuch(n)
  flips = 0
  pc = 0
  (1..n).to_a.permutation.to_a.each do |perm|
    flips = max(flips, flip(perm))
    if pc < 30
      print perm.to_s + "\n"
      pc += 1
    end
  end
  flips
end

def flip(l)
  c = 0
  while l[0] != 1 do
    l = reverse(l[0], l)
    c += 1
  end
  c
end
fannkuch(10)
