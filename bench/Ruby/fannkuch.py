def reverse(n, l)
    return [] if n <= 0
    l[0..n].reverse! + l[n+1..l.size()]
end
reverse(2, [1,2,3])
