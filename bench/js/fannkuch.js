function reverse(a, e) {
    var s = 0;
    e = e - 1;
    while ( s < e ){
        var aux = a[s];
        a[s] = a[e];
        a[e] = aux;
        s = s + 1;
        e = e - 1;
    }
    return a;
}

function permutator(inputArr) {
  var results = [];

  function permute(arr, memo) {
    var cur, memo = memo || [];

    for (var i = 0; i < arr.length; i++) {
      cur = arr.splice(i, 1);
      if (arr.length === 0) {
        results.push(memo.concat(cur));
      }
      permute(arr.slice(), memo.concat(cur));
      arr.splice(i, 0, cur[0]);
    }

    return results;
  }

  return permute(inputArr);
}

function flip(p){
    var count = 0;
    while (p[0] !== 1){
        reverse(p, p[0]);
        count = count + 1;
    }
    return count;
}
function max(a, b){
    if (a > b){
        return a;
    }
    else {
        return b;
    }
}

function gen_list(n){
    var l = [];
    var i = 0;
    while ( n !== 0 ){
        l[i] = i + 1;
        i = i + 1;
        n = n - 1;
    }
    return l;
}
function fannkuch(n){
    var flips = 0;
    var print = 0;
    n = gen_list(n);
    n = permutator(n);
    var l = n.length;
    var i = 0;
    while (i < l){
        var y = n[i];
        var f = flip(y);
        flips = max(flips, f);
        if ( print < 30 ){
            console.log(y);
            print = print + 1;
        }
        else {}
        i = i + 1;
    }
    return flips;
}
console.log(fannkuch(10));
