function reverse(a, e) {
    var s = 0;
    e = e - 1;
    while ( s < e ){
        var aux = a[s];
        a[s] = a[e];
        a[e] = aux;
        s = s + 1;
        e = e - 1;
    }
    return a;
}
function flip(p){
    var count = 0;
    while (p[0] !== 1){
        reverse(p, p[0]);
        count = count + 1;
    }
    return count;
}
function max(a, b){
    if (a > b){
        return a;
    }
    else {
        return b;
    }
}

function gen_list(n){
    var l = [];
    var i = 0;
    while ( n !== 0 ){
        l[i] = i + 1;
        i = i + 1;
        n = n - 1;
    }
    return l;
}
function fannkuch(n){
    var flips = 0;
    var print = 0;
    n = gen_list(n);
    n = n.permutations();
    var l = n.len();
    var i = 0;
    while (i < l){
        if ( print < 30 ){
                console.log(n[i]);
                print = print + 1;
        }
        else {}
        var f = flip(n[i]);
        flips = max(flips, f);
        i = i + 1;
    }
    return flips;
}
console.log(fannkuch(3));
