def reverse(n, l):
        if n <= 0:
            return []
        return l[:n][::-1] + l[n:]

def all_perms(elements):
    if len(elements) <=1:
        yield elements
    else:
        for perm in all_perms(elements[1:]):
            for i in range(len(elements)):
                # nb elements[0:1] works in both string and list contexts
                yield perm[:i] + elements[0:1] + perm[i:]

def fannkuch(n):
  flips = 0
  pc = 0
  for p in all_perms(list(xrange(n + 1))[1:]):
      flips = max(flips, flip(p))
      if pc < 30:
          print p
          pc += 1
  print flips

def flip(l):
    c = 0
    while l[0] != 1:
        l = reverse(l[0], l)
        c += 1
    return c

fannkuch(9)
